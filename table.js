is_editing = false;

function addRow() {
    var tbody = document.querySelector('#the_table tbody');
    let row = document.createElement("tr");
    row.addEventListener('dblclick', function () {
        startEdit(row);
    });

    let first_cell = document.createElement("td");
    let second_cell = document.createElement("td");
    let third_cell = document.createElement("td");
    let fourth_cell = document.createElement("td");
    fourth_cell.className = "delete";
    fourth_cell.appendChild(document.createTextNode("-"));
    fourth_cell.addEventListener('click', function (event) {
        event.stopPropagation();
        tbody.removeChild(row);
        if (is_editing == row) {
            is_editing = false;
        }
    });

    row.appendChild(first_cell);
    row.appendChild(second_cell);
    row.appendChild(third_cell);
    row.appendChild(fourth_cell);


    tbody.appendChild(row);
    startEdit(row);
}

function startEdit(row) {
    if (is_editing) {
        stopEdit();
    }
    is_editing = row;
    let cell_value_1 = row.childNodes[0].innerHTML;
    let cell_value_2 = row.childNodes[1].innerHTML;
    let cell_value_3 = row.childNodes[2].innerHTML;
    row.childNodes[0].innerHTML = '<input type"text" name="first_cell" class="inline_edit" value="' + cell_value_1 + '" />';
    row.childNodes[1].innerHTML = '<input type"text" name="second_cell" class="inline_edit" value="' + cell_value_2 + '" />';
    row.childNodes[2].innerHTML = '<input type"text" name="third_cell" class="inline_edit" value="' + cell_value_3 + '" />';

    var stopEditEvent = function (event) {
        switch (event.key) {
            case 'Enter':
                stopEdit();
                break;
            case 'ArrowDown':
                if (is_editing.nextSibling) {
                    startEdit(is_editing.nextSibling);
                } else {
                    addRow();
                }
                break;
            case 'ArrowUp':
                if (is_editing.previousSibling) {
                    startEdit(is_editing.previousSibling);
                }
                break;
            case 'Delete':
                
        }
    };

    var first_cell = document.querySelector('input[name="first_cell"]');
    first_cell.addEventListener('keydown', stopEditEvent);
    document.querySelector('input[name="second_cell"]').addEventListener('keydown', stopEditEvent);
    document.querySelector('input[name="third_cell"]').addEventListener('keydown', stopEditEvent);
    first_cell.focus();
}

function stopEdit() {
    if (is_editing) {
        var cell_value_1 = is_editing.childNodes[0].childNodes[0].value;
        var cell_value_2 = is_editing.childNodes[1].childNodes[0].value;
        var cell_value_3 = is_editing.childNodes[2].childNodes[0].value;
        is_editing.childNodes[0].innerHTML = cell_value_1;
        is_editing.childNodes[1].innerHTML = cell_value_2;
        is_editing.childNodes[2].innerHTML = cell_value_3;

        is_editing = false;
    }
}

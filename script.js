var formFields = [
    'name',
    'email',
    'date_birthday',
    'password',
    'repeat_password',
    'description',
];
var formElements = {};
function validateField(fieldName) {
    var return_data = {};
    var element = formElements[fieldName];
    var value = element.value;
    switch (fieldName) {
        case 'name':
            return_data = validateName(value);
            break;
        case 'email':
            return_data = validateEmail(value);
            break;
        case 'date_birthday':
            return_data = validateDate(value);
            break;
        case 'password':
            return_data = validatePassword(value);
            break;
        case 'repeat_password':
            return_data = validateRepeatPassword(value, formElements.password.value);
            break;
        case 'description':
            return_data = validateDescription(value);
            break;
    }
    if (return_data.is_error) {
        addError(element);
    } else {
        removeError(element);
    }
    return return_data;
}

function validateName(value) {
    var return_data = {
        is_error: true,
        error: '',
    };
    if (value.length < 2) {
        return_data.error = 'Imię i nazwisko muszą mieć przynajmniej 2 znaki.';
    } else {
        return_data.is_error = false;
    }
    return return_data;
}

function validateEmail(value) {
    var return_data = {
        is_error: true,
        error: '',
    };
    if (!value.length) {
        return_data.error = 'Email nie może być pusty.';
    } else if (!validateEmailByPattern(value)) {
        return_data.error = 'Nieprawidłowy format adresu e-mail.';
    } else {
        return_data.is_error = false;
    }
    return return_data;
}

function validateDate(value) {
    var return_data = {
        is_error: true,
        error: '',
    };
    if (!value.length) {
        return_data.error = 'Data urodzenia nie może być pusta.';
    } else {
        return_data.is_error = false;
    }
    return return_data;
}

function validatePassword(value) {
    var return_data = {
        is_error: true,
        error: '',
    };
    if (!value.length) {
        return_data.error = 'Hasło nie może być puste.';
    } else if (value.length < 8) {
        return_data.error = 'Hasło musi mieć minimum 8 znaków.';
    } else if (getNumberCount(value) < 1) {
        return_data.error = 'Hasło musi zawierać conajmniej jedną cyfrę.';
    } else {
        return_data.is_error = false;
    }
    return return_data;
}
function validateRepeatPassword(value, password) {
    var return_data = {
        is_error: true,
        error: '',
    };
    if (value !== password) {
        return_data.error = 'Hasła się nie zgadzają.';
    } else {
        return_data.is_error = false;
    }
    return return_data;
}

function validateDescription(value) {
    var return_data = {
        is_error: true,
        error: 0,
    };
    if (!value.length) {
        return_data.error = 'Opis nie może być pusty.';
    } else if (value.length > 100) {
        return_data.error = 'Opis może mieć maksymalnie 100 znaków.';
    } else {
        return_data.is_error = false;
    }
    return return_data;
}

function initForm() {
    for (var index = 0; index < formFields.length; ++index) {
        let fieldName = formFields[index];
        formElements[fieldName] = document.querySelector('*[name="' + fieldName + '"]');
        formElements[fieldName].addEventListener('blur', function () {
            validateField(fieldName);
        });
    }
    var form = document.querySelector('form[name="the_form"]');
    var formErrors = document.querySelector('#form_errors');
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        formErrors.className = "";
        formErrors.innerHTML = "";
        var error_occured = false;
        for (var index = 0; index < formFields.length; ++index) {
            var fieldName = formFields[index];
            var result = validateField(fieldName);
            if (result.is_error) {
                error_occured = true;
                var html = '<div>' + result.error + '</div>';
                formErrors.innerHTML += html;
            }
        }
        if (error_occured) {
            formErrors.className = "error";
        }
    });
}

function addError(element) {
    element.className = "error";
    element.parentNode.parentNode.childNodes[1].className = "label error_label";
}

function removeError(element) {
    element.className = "";
    element.parentNode.parentNode.childNodes[1].className = "label";
}
function validateEmailByPattern(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function getNumberCount(str) {
    return str.replace(/[^0-9]/g, "").length;
}
initForm();